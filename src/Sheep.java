public class Sheep {

    enum Animal {
        sheep, goat
    }

    public static void main(String[] param) {
        // for debugging
    }

    public static void reorder(Animal[] animals) {
        int goatCount = countGoats(animals);
        for (int i = 0; i < animals.length; i++) {
            if (goatCount != 0) {
                animals[i] = Animal.goat;
                goatCount--;
            } else {
                animals[i] = Animal.sheep;
            }
        }
    }

    private static int countGoats(Animal[] animals) {
        int goatCount = 0;
        for (Animal x : animals) {
            if (x == Animal.goat) {
                goatCount++;
            }
        }
        return goatCount;
    }
}